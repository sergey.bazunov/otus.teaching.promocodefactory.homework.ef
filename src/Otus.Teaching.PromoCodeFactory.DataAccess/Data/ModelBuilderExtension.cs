﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class ModelBuilderExtension
    {
        /// <summary>
        /// Feed database with test data from FakeDataFactory
        /// </summary>        
        public static void SeedDatabase(this ModelBuilder builder)
        {
            builder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            //builder.Entity<Role>().HasData(FakeDataFactory.Roles);            
            //builder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
            //builder.Entity<Customer>().HasData(FakeDataFactory.Customers);
        }
    }
}
