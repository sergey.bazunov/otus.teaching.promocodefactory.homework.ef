﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DataContext : DbContext
    {
        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }
        public DbSet<CustomerPromoCode> CustomerPromoCodes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=PromoCodeFactory.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerPreference>(cr =>
            {
                cr.HasKey(k => new { k.CustomerId, k.PreferenceId });
            });

            modelBuilder.Entity<CustomerPromoCode>(cpc =>
            {
                cpc.HasKey(k => new { k.CustomerId, k.PromoCodeId });
            });

            modelBuilder.Entity<Employee>(e =>
            {
                e.HasMany<Role>()
                .WithOne(em => em.Employee).HasForeignKey(k => k.EmployeeId);
            });

            modelBuilder.Entity<Employee>().HasData(new List<Employee>() {
                new Employee()
                {
                    Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                    Email = "owner@somemail.ru",
                    FirstName = "Иван",
                    LastName = "Сергеев",
                    AppliedPromocodesCount = 5
                },
                new Employee()
                {
                    Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                    Email = "andreev@somemail.ru",
                    FirstName = "Петр",
                    LastName = "Андреев",
                    AppliedPromocodesCount = 10
                }
            });

            base.OnModelCreating(modelBuilder);
            //modelBuilder.SeedDatabase();
        }
    }
}
