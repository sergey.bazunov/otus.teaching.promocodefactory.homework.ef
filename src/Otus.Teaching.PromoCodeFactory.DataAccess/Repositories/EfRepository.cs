﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    /// <summary>
    /// Repository based on DataContext (DbContext)
    /// </summary>    
    // https://stackoverflow.com/questions/33041113/c-sharp-entity-framework-correct-use-of-dbcontext-class-inside-your-repository

    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly DataContext context;

        public EfRepository(DataContext context)
        {            
            this.context = context;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await context.Set<T>().AsNoTracking().ToListAsync();
        }

        public async Task<IEnumerable<T>> GetAllAsync(Func<T, bool> predicate)
        {
            var items = context.Set<T>().Where(predicate);
            return await Task.FromResult(items.ToList());
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var e = await context.Set<T>().AsNoTracking().FirstOrDefaultAsync(item => item.Id == id);
            if (e == null)
            {
                return await Task.FromResult<T>(null);
            }

            return await Task.FromResult(e);
        }

        public async Task<T> Create(T NewEntity)
        {
            await context.Set<T>().AddAsync(NewEntity);
            bool ok = true;
            try
            {
                await context.SaveChangesAsync();
            }
            catch
            {
                // write log is here
                ok = false;
            }

            if (ok)
                return await Task.FromResult<T>(NewEntity);
            else
                return await Task.FromResult<T>(null);
        }

        public async Task<T> Update(T Element)
        {
            var e = await context.Set<T>().FindAsync(Element);
            if (e == null)
            {
                return await Task.FromResult<T>(null);
            }

            context.Set<T>().Update(e);
            bool ok = true;
            try
            {
                await context.SaveChangesAsync();
            }
            catch
            {
                // write log is here
                ok = false;
            }

            if (ok)
                return await Task.FromResult<T>(e);
            else
                return await Task.FromResult<T>(null);
        }

        public async Task<T> Delete(Guid id)
        {
            var e = await GetByIdAsync(id);            
            if (e == null)
            {
                return await Task.FromResult<T>(null);
            }
            context.Set<T>().Remove(e);
            
            bool ok = true;
            try
            {
                await context.SaveChangesAsync();
            }
            catch
            {
                // write log is here
                ok = false;
            }

            if (ok)
                return await Task.FromResult<T>(e);
            else
                return await Task.FromResult<T>(null);
        }
    }
}
