﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    //https://gitlab.com/devgrav/otus.teaching.promocodefactory/-/wikis/Homework-2
    public class InMemoryRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected IList<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<IEnumerable<T>> GetAllAsync(Func<T, bool> predicate)
        {
            return Task.FromResult(Data.Where(predicate).AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> Create(T NewEntity)
        {
            if (Data.Any(e => e.Id == NewEntity.Id))
            {
                return Task.FromResult<T>(null);
            }

            IEnumerable<T> NewData = Data.Append(NewEntity).ToList();
            Data = new List<T>(NewData);

            return Task.FromResult(NewEntity);
        }

        public Task<T> Update(T Element)
        {
            List<T> items = Data.ToList();
            T e = items.Find(e => e.Id == Element.Id);

            if (e == null)
            {
                return Task.FromResult<T>(null);
            }

            items.Remove(e);
            items.Add(Element);
            Data = new List<T>(items);
            return Task.FromResult(Element);
        }

        public Task<T> Delete(Guid id)
        {
            List<T> items = Data.ToList();
            T e = items.Find(e => e.Id == id);

            if (e == null)
            {
                return Task.FromResult<T>(null);
            }

            items.Remove(e);
            Data = new List<T>(items);
            return Task.FromResult(e);
        }
    }
}