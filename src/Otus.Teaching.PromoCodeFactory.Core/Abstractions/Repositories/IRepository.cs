﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<IEnumerable<T>> GetAllAsync(Func<T, bool> predicate);
        
        Task<T> GetByIdAsync(Guid id);        

        Task<T> Create(T NewEntity);

        Task<T> Update(T Element);

        Task<T> Delete(Guid id);
    }
}